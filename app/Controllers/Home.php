<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$all_pekan = [
		[
				'nama' => 'Seremban',
				'gambar' => 'https://images.unsplash.com/photo-1570036340722-b301922be4b5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bmVnZXJpJTIwc2VtYmlsYW4lMjBzZXJlbWJhbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, error veritatis. Similique cum eum sunt rem, quos a consectetur, ad architecto sed nulla quo animi odio magnam neque, autem perspiciatis!'
			], [
				'nama' => 'Gemas',
				'gambar' => 'https://images.unsplash.com/photo-1571828322495-345f43838c61?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fG5lZ2VyaSUyMHNlbWJpbGFuJTIwc2VyZW1iYW58ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, error veritatis. Similique cum eum sunt rem, quos a consectetur, ad architecto sed nulla quo animi odio magnam neque, autem perspiciatis!'
			], [
				'nama' => 'Port Dickson',
				'gambar' => 'https://images.unsplash.com/photo-1571828322495-345f43838c61?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fG5lZ2VyaSUyMHNlbWJpbGFuJTIwc2VyZW1iYW58ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, error veritatis. Similique cum eum sunt rem, quos a consectetur, ad architecto sed nulla quo animi odio magnam neque, autem perspiciatis!'
			], [
				'nama' => 'Rembau',
				'gambar' => 'https://images.unsplash.com/photo-1571828322495-345f43838c61?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fG5lZ2VyaSUyMHNlbWJpbGFuJTIwc2VyZW1iYW58ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, error veritatis. Similique cum eum sunt rem, quos a consectetur, ad architecto sed nulla quo animi odio magnam neque, autem perspiciatis!'
			], [
				'nama' => 'Kuala Pilah',
				'gambar' => 'https://images.unsplash.com/photo-1571828322495-345f43838c61?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTJ8fG5lZ2VyaSUyMHNlbWJpbGFuJTIwc2VyZW1iYW58ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60',
				'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quos, error veritatis. Similique cum eum sunt rem, quos a consectetur, ad architecto sed nulla quo animi odio magnam neque, autem perspiciatis!'
		]
				];

		return view('homepage', [ 'all_pekan' => $all_pekan]);
	}

	function edit() {
		echo "<h1> WOI...</h1>";
	}

	function add() {
		echo "<h1> Borang Untuk Add La </h1>";
	}
}
